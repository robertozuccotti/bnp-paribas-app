//
//  CheckLoginViewController.swift
//  Prova
//
//  Created by Interlem on 24/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

class CheckLoginViewController: UIViewController {

    let me = self
    let storeUtil = StoreUtil()
    let restClient = RestClient()
    let user = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let indicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
//        indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
//        indicator.center = view.center
//        view.addSubview(indicator)
//        indicator.bringSubview(toFront: view)
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        if self.storeUtil.userIsLogged() {
            let userInfo = storeUtil.getLoggedUserInfo()
            
            user.username = userInfo.username
            user.password = userInfo.password
            user.company = userInfo.company
            
            restClient.login(user: user, callback: {(success: Bool, message: String) in
                if success {
                    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController")
                    self.storeUtil.setLastMenu(menu: "To do")
                    OperationQueue.main.addOperation {
                        self.present(nextViewController!, animated:true, completion:nil)
                    }
                }
                else {
                    OperationQueue.main.addOperation {
                        let alert = UIAlertController(title: "Attenzione", message: "Login fallito", preferredStyle: UIAlertControllerStyle.alert)
                        let retryAction = UIAlertAction(title: "Riprova", style: UIAlertActionStyle.cancel) {
                            UIAlertAction in
                            let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "loginView")
                            OperationQueue.main.addOperation {
                                self.present(loginViewController!, animated:true, completion:nil)
                            }
                        }
                        alert.addAction(retryAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        } else {
            let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "loginView")
            OperationQueue.main.addOperation {
                self.present(loginViewController!, animated:true, completion:nil)
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
