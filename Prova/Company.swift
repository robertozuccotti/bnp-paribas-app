//
//  Company.swift
//  Prova
//
//  Created by Interlem on 19/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class Company: NSObject {

    var _name: String
    var _workflow: Array<String>
    
    override required init() {
        self._name = ""
        self._workflow = []
    }
    
}
