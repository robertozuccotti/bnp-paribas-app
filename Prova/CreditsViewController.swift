//
//  SecondViewController.swift
//  Prova
//
//  Created by Interlem on 09/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class CreditsViewController : UIViewController, MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var menuItem: UIBarButtonItem!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var buildNumberLabel: UILabel!
    @IBOutlet weak var helpdeskButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg_blu")!)
        
        if self.revealViewController() != nil {
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        menuItem.target = revealViewController()
        menuItem.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        
        versionLabel.text = appName + " " + versionNumber
        buildNumberLabel.text = "Build: " + buildNumber + " - 09/02/2017"

    }
    
    @IBAction func onHelpdeskButton(_ sender: Any) {
        
        let mail = configuratedMail()
        if MFMailComposeViewController.canSendMail() {
            self.present(mail, animated: true, completion: nil)
        }
        else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    func configuratedMail() -> MFMailComposeViewController {
        
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients(["helpdesk@interlem.it"])
        
        return mail
        
    }
    
    func showSendMailErrorAlert() {
    
        let alert = UIAlertView(title: "Impossibile inviare la mail", message: "Il tuo dispositivo non può inviare mail. Controlla le impostazioni della mail e riprova.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        
        case MFMailComposeResult.cancelled:
            break
            
        case MFMailComposeResult.saved:
            let alert = UIAlertView(title: "", message: "L'e-mail è stata salvata nelle bozze", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            break
            
        case MFMailComposeResult.sent:
            let alert = UIAlertView(title: "", message: "L'e-mail è stata inviata con successo", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            break
            
        case MFMailComposeResult.failed:
            let alert = UIAlertView(title: "", message: "Impossibile inviare l'e-mail, riprovare", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            break
            
        }
        self.dismiss(animated: true, completion: nil)
    
    }

}
