//
//  DetailViewCellModel.swift
//  Prova
//
//  Created by Interlem on 07/02/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class DetailViewCellModel {
    
    var label: String = ""
    var text: String = ""
    var approved: Bool = false
    var required: Bool = false
    var type: String = ""
    var editable: Bool = false
    var url: String = ""
    var key: String = ""

}
