//
//  SecondViewController.swift
//  Prova
//
//  Created by Interlem on 09/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

class DetailViewController : UIViewController {
    
    @IBOutlet weak var myTableDetail: UITableView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIButton!
    
    var myDetail = [DetailViewCellModel]()
    
    var taskSelected = Task()
    let storeUtil = StoreUtil()
    let restClient = RestClient()
    let restUtil = RestUtil()
    var mySession = Session()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        fillData()
        
    }
    
    func fillData() {
        
        self.taskSelected = self.storeUtil.getSelectedTask()
        
        let config = self.restUtil.getConfigs()
        let map = (config?["MapDetailView"] as! NSDictionary)[taskSelected._name] as?[NSDictionary]
        
        self.restClient.getCaseVariables(caseId: taskSelected._caseId) {
            (caseModel: Case, success: Bool, message: String) in
            if (success) {
                if self.storeUtil.getLastMenu() == "Done" {
                    print("jaa")
                    for (field, value) in caseModel._data {
                        let key = field
                        let label = field
                        let required = false
                        let type = "textfield"
                        let editable = false
                        if caseModel._data[key] != nil {
                            let model = DetailViewCellModel()
                            model.label = label
                            model.required = required
                            model.type = type
                            model.text = value
                            model.editable = editable
                            model.key = key
                            
                            self.myDetail.append(model)
                        }
                    }
                }
                else {
                    for field in map! {
                        let key = field["Key"] as! String
                        let label = field["Label"] as! String
                        let required = field["Required"] as! Bool
                        let type = field["Type"] as! String
                        let editable = field["Editable"] as! Bool
                        
                        if (type == "link")
                        {
                            let documentList = caseModel._document
                            for doc in documentList {
                                if doc._name == key {
                                    let model = DetailViewCellModel()
                                    model.label = label
                                    model.required = required
                                    model.type = type
                                    model.text = doc._filename
                                    model.url = doc._url
                                    model.editable = editable
                                    model.key = key
                                    
                                    self.myDetail.append(model)
                                }
                            }
                        }
                        else
                        {
                            if caseModel._data[key] != nil {
                                let value = caseModel._data[key]! as String
                                
                                let model = DetailViewCellModel()
                                model.label = label
                                model.required = required
                                model.type = type
                                model.text = value
                                model.editable = editable
                                model.key = key
                                
                                self.myDetail.append(model)
                            }
                        }
                    }
                }
                
                DispatchQueue.main.async{
                    self.myTableDetail.reloadData()
                }
            } else {
                print(message)
            }
        }
        
        myTableDetail.delegate = self
        myTableDetail.dataSource = self
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        let me = self
        let nextViewController = me.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController")
        
        OperationQueue.main.addOperation{
            me.present(nextViewController!, animated:true, completion:nil)
        }
    }
    
    @IBAction func onSave(_ sender: Any) {
        restClient.getUserSession { (data: Session, success: Bool, message: String) in
            if success {
                self.mySession = data
                self.restClient.assignTaskToUser(taskId: self.taskSelected._id, userId: self.mySession.user_id, callback: {
                    (data: Array<Task>, success: Bool, message: String) in
                    if success {
                        var form = [[String: String]]()
                        var field = [String:String]()
                        var payload = [String:Any]()
                        for myField in self.myDetail {
                            if myField.editable {
                                field["name"] = myField.key
                                if myField.type == "switch" {
                                    field["value"] = myField.approved ? "Si" : "No"
                                }
                                else {
                                    field["value"] = myField.text
                                }
                                form.append(field)
                            }
                        }
                        payload["state"] = "completed"
                        payload["variables"] = form
                        if success {
                            self.restClient.executeTask(taskId: self.taskSelected._id, myPayload: payload, callback: {
                                (data: Array<Task>, success: Bool, message: String) in
                                if success {
                                    let alert = UIAlertController(title: "Case "+self.taskSelected._caseId, message: "Le informazioni sono state inviate", preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                                        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController")
                                        OperationQueue.main.addOperation {
                                            self.present(nextViewController!, animated:true, completion:nil)
                                        }
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch myDetail[indexPath.row].type {
        case "switch":
            let cell = Bundle.main.loadNibNamed("SwitchTableViewCell", owner: self, options: nil)?.first as! SwitchTableViewCell
            cell.setupWithModel(model: myDetail[indexPath.row])
            cell.delegate = self
            return cell
        case "textarea":
            let cell = Bundle.main.loadNibNamed("TextAreaTableViewCell", owner: self, options: nil)?.first as! TextAreaTableViewCell
            cell.setupWithModel(model: myDetail[indexPath.row])
            return cell
        case "link":
            let cell = Bundle.main.loadNibNamed("LinkTableViewCell", owner: self, options: nil)?.first as! LinkTableViewCell
            cell.setupWithModel(model: myDetail[indexPath.row])
            return cell
        default:
            let cell = Bundle.main.loadNibNamed("StandardTableViewCell", owner: self, options: nil)?.first as! StandardTableViewCell
            cell.setupWithModel(model: myDetail[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myDetail.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch myDetail[indexPath.row].type {
        case "switch":
            return 79
        case "textarea":
            return 181
        case "link":
            return 76
        default:
           return 78
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let record = myDetail[indexPath.row]
        if (record.type == "link" && record.url != "") {
            UIApplication.shared.openURL(URL(string: record.url)!)
        }
        
    }
}

extension DetailViewController: SwitchViewCellDelegate {
    func didTappedSwitch(cell: SwitchTableViewCell) {
        let indexPath = myTableDetail.indexPath(for: cell)
        myDetail[(indexPath?.row)!].approved = cell.switch.isOn
    }
}
