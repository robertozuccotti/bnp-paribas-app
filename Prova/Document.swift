//
//  Document.swift
//  Prova
//
//  Created by Interlem on 07/02/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class Document : NSObject{

    var _id: String = ""
    var _creationDate: String = ""
    var _author: String = ""
    var _contentMimetype: String = ""
    var _caseId: String = ""
    var _file: String = ""
    var _name: String = ""
    var _filename: String = ""
    var _url: String = ""
    var _fileName: String = ""
    var _description: String = ""
    var _contentStorageId: String = ""
    var _index: String = ""
    var _submittedBy: String = ""
    var _isInternal: String = ""
    var _version: String = ""
    
}
