//
//  LinkTableViewCell.swift
//  Prova
//
//  Created by Interlem on 08/02/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

class LinkTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var linkImage: UIImageView!
    @IBOutlet weak var progress: UIProgressView!
    
    func setupWithModel(model: DetailViewCellModel) {
        label.text = model.label
        
        if model.url != "" {
            linkImage.image = #imageLiteral(resourceName: "Download-50")
        } else {
            linkImage.image = #imageLiteral(resourceName: "Delete-50")
        }
        progress.transform = CGAffineTransform(scaleX: 1.0, y: 0.5)
        self.selectionStyle = .none
    }
}
