//
//  SecondViewController.swift
//  Prova
//
//  Created by Interlem on 09/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation
import UIKit

class ListViewController : UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    let restClient = RestClient()
    let storeUtil = StoreUtil()
    let restUtil = RestUtil()
    let utility = Utility()
    var myList: Array<Task> = []
    var mySession = Session()
    var selectedMenu = ""
    var codeSelectedMenu = ""
    var refreshControl: UIRefreshControl!
    var page = 0
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var callbackComplete = true
    var tableViewController = UITableViewController(style: .plain)
    var scrolledToEnd = false
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var SortButton: UIBarButtonItem!
    
    func refresh(sender:AnyObject) {
        var sort = self.SortButton.title
        if sort != "Sort" {
            sort = self.utility.getSortString(selectedSort: sort!)
        }
        else {
            sort = nil
        }
        var filter = self.searchField.text
        if filter == "" {
            filter = nil
        }
        else {
            filter = "&s=" + (filter?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!
        }
        if self.utility.checkMenuStatic(menuSelected: self.selectedMenu) {
            self.restClient.getWFTask(WFCode: self.codeSelectedMenu, pageNumber: 0, pageSize: self.myList.count < 15 ? 15 : self.myList.count, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                (data: Array<Task>, success: Bool, message: String) in
                if (success) {
                    self.myList = data
                } else {
                    print(message)
                }
                DispatchQueue.main.async{
                    self.TableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        } else {
            self.restClient.getTasks(method: self.selectedMenu, pageNumber: 0, pageSize: self.myList.count < 15 ? 15 : self.myList.count, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                (data: Array<Task>, success: Bool, message: String) in
                if (success) {
                    self.myList = data
                } else {
                    print(message)
                }
                DispatchQueue.main.async{
                    self.TableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
        }
        self.scrolledToEnd = false
    }
    
    
    override func viewDidLoad() {
        
        if self.revealViewController() != nil {
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(refresh), for: UIControlEvents.valueChanged)
        TableView.addSubview(refreshControl)
        
        selectedMenu = storeUtil.getLastMenu()
        
        self.title = selectedMenu
        
        let myLoggedInfo = storeUtil.getLoggedUserInfo()
        
        if utility.checkMenuStatic(menuSelected: selectedMenu) {
            codeSelectedMenu = storeUtil.getWFCode(company: myLoggedInfo.company, WF: selectedMenu)
        }
        
        restClient.getUserSession { (data: Session, success: Bool, message: String) in
            self.mySession = data
            if self.utility.checkMenuStatic(menuSelected: self.selectedMenu) {
                self.restClient.getWFTask(WFCode: self.codeSelectedMenu, pageNumber: self.page, pageSize: 15, userId: self.mySession.user_id, callback: {
                    (data: Array<Task>, success: Bool, message: String) in
                    if (success) {
                        self.myList = data
                        DispatchQueue.main.async{
                            self.TableView.reloadData()
                        }
                        self.page += 1
                    } else {
                        print(message)
                    }
                })
            } else {
                self.restClient.getTasks(method: self.selectedMenu, pageNumber: self.page, pageSize: 15, userId: self.mySession.user_id, callback: {
                    (data: Array<Task>, success: Bool, message: String) in
                    if (success) {
                        self.myList = data
                        DispatchQueue.main.async{
                            self.TableView.reloadData()
                        }
                        self.page += 1
                    } else {
                        print(message)
                    }
                })
            }
        }
        
        menuButton.target = revealViewController()
        menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListViewCell") as! ListViewCell
        let myTask = myList[indexPath.row]
        cell.myTitle.text = myTask._displayName
        cell.mySubtitle.text = myTask._caseId + " - " + myTask._processName
        cell.myStatus.text = myTask._name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let taskId = self.myList[indexPath.row]._id
        let userId = self.mySession.user_id
        
        let takeAction = UITableViewRowAction(style: .normal, title: "Take") {
            (UITableViewRowAction, IndexPath) in
            
            self.restClient.assignTaskToUser(taskId: taskId, userId: userId, callback: {
                (data: Array<Task>, success: Bool, message: String) in
            })
            
            self.myList.remove(at: indexPath.row)
            self.TableView.reloadData()
        }
        
        takeAction.backgroundColor = UIColor(red: 48/255, green: 173/255, blue: 35/255, alpha: 1)
        
        let releaseAction = UITableViewRowAction(style: .normal, title: "Release") {
            (UITableViewRowAction, IndexPath) in
            
            self.restClient.assignTaskToUser(taskId: taskId, userId: "", callback: {
                (data: Array<Task>, success: Bool, message: String) in
            })
            
            self.myList.remove(at: indexPath.row)
            self.TableView.reloadData()
        }
        
        releaseAction.backgroundColor = UIColor(red: 229/255, green: 78/255, blue: 66/255, alpha: 1)
        
        let hideAction = UITableViewRowAction(style: .normal, title: "Hide") {
            (UITableViewRowAction, IndexPath) in
            
            self.restClient.hideTask(taskId: taskId, userId: userId, callback: {
                (data: Array<Task>, success: Bool, message: String) in
            })
            
            self.myList.remove(at: indexPath.row)
            self.TableView.reloadData()
        }
        
        hideAction.backgroundColor = UIColor(red: 254/255, green: 211/255, blue: 46/255, alpha: 1)
        
        let retrieveAction = UITableViewRowAction(style: .normal, title: "Retrieve") {
            (UITableViewRowAction, IndexPath) in
            
            self.restClient.retrieveTask(taskId: taskId, userId: userId, callback: {
                (data: Array<Task>, success: Bool, message: String) in
            })
            
            self.myList.remove(at: indexPath.row)
            self.TableView.reloadData()
        }
        
        retrieveAction.backgroundColor = UIColor(red: 41/255, green: 145/255, blue: 214/255, alpha: 1)
        
        var returnAction: Array<UITableViewRowAction> = []
        
        switch self.storeUtil.getLastMenu() {
        case "To do":
            returnAction = [takeAction, releaseAction, hideAction]
        case "My tasks":
            returnAction = [releaseAction, hideAction]
        case "Available tasks":
            returnAction = [takeAction, hideAction]
        case "Hidden":
            returnAction = [retrieveAction]
        case "Done":
            returnAction = []
        default:
            returnAction = [takeAction, releaseAction, hideAction]
        }
        
        return returnAction
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = self.myList[indexPath.row]
        self.storeUtil.setSelectedTask(task: task)
        
        let config = self.restUtil.getConfigs()
        let map = config?["MapDetailView"] as! NSDictionary
        
        if map[self.myList[indexPath.row]._name] != nil || self.storeUtil.getLastMenu() == "Done"{
            let me = self
            let nextViewController = me.storyboard?.instantiateViewController(withIdentifier: "detailView")
            
            OperationQueue.main.addOperation{
                me.present(nextViewController!, animated:true, completion:nil)
            }
        } else {
            OperationQueue.main.addOperation {
                let alert = UIAlertController(title: "Errore", message: "Questo dettaglio è ancora in fase di sviluppo", preferredStyle: UIAlertControllerStyle.alert)
                let retryAction = UIAlertAction(title: "Continua", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                }
                alert.addAction(retryAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func startSearch(_ sender: Any) {
        self.view.endEditing(true)
        self.page = 1
        
        var filter = self.searchField.text
        var sort = self.SortButton.title
        
        if filter == "" {
            filter = nil
        }
        else {
            filter = "&s=" + (filter?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!
        }
        
        if sort != "Sort" {
            sort = self.utility.getSortString(selectedSort: sort!)
        }
        else {
            sort = nil
        }
        
        if self.utility.checkMenuStatic(menuSelected: self.selectedMenu) {
            self.restClient.getWFTask(WFCode: self.codeSelectedMenu, pageNumber: 0, pageSize: 15, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                (data: Array<Task>, success: Bool, message: String) in
                if (success) {
                    self.myList = data
                    DispatchQueue.main.async{
                        self.TableView.reloadData()
                    }
                } else {
                    print(message)
                }
            })
        } else {
            self.restClient.getTasks(method: self.selectedMenu, pageNumber: 0, pageSize: 15, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                (data: Array<Task>, success: Bool, message: String) in
                if (success) {
                    self.myList = data
                    DispatchQueue.main.async{
                        self.TableView.reloadData()
                    }
                } else {
                    print(message)
                }
            })
        }
        self.scrolledToEnd = false
    }
    
    @IBAction func changeFilter(_ sender: Any) {
        if self.searchField.text == "" {
            self.page = 1
            var sort = self.SortButton.title
            if sort != "Sort" {
                sort = self.utility.getSortString(selectedSort: sort!)
            }
            else {
                sort = nil
            }
            if self.utility.checkMenuStatic(menuSelected: self.selectedMenu) {
                self.restClient.getWFTask(WFCode: self.codeSelectedMenu, pageNumber: 0, pageSize: 15, userId: self.mySession.user_id, sort: sort, callback: {
                    (data: Array<Task>, success: Bool, message: String) in
                    if (success) {
                        self.myList = data
                        DispatchQueue.main.async{
                            self.TableView.reloadData()
                        }
                    } else {
                        print(message)
                    }
                })
            } else {
                self.restClient.getTasks(method: self.selectedMenu, pageNumber: 0, pageSize: 15, userId: self.mySession.user_id, sort: sort, callback: {
                    (data: Array<Task>, success: Bool, message: String) in
                    if (success) {
                        self.myList = data
                        DispatchQueue.main.async{
                            self.TableView.reloadData()
                        }
                    } else {
                        print(message)
                    }
                })
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height && self.myList.count >= 15 && !self.scrolledToEnd{
            
            if callbackComplete {
                
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
                activityIndicator.color = UIColor.black
                activityIndicator.center.x = view.center.x
                activityIndicator.center.y = view.bounds.size.height*0.95
//                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
                view.addSubview(activityIndicator)
                
                DispatchQueue.main.async{
                    self.activityIndicator.startAnimating()
                }
                callbackComplete = false
                
                restClient.getUserSession { (data: Session, success: Bool, message: String) in
                    self.mySession = data
                    var filter = self.searchField.text
                    var sort = self.SortButton.title
                    
                    if filter == "" {
                        filter = nil
                    }
                    else {
                        filter = "&s=" + (filter?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!
                    }
                    
                    if sort != "Sort" {
                        sort = self.utility.getSortString(selectedSort: sort!)
                    }
                    else {
                        sort = nil
                    }
                    if self.utility.checkMenuStatic(menuSelected: self.selectedMenu) {
                        self.restClient.getWFTask(WFCode: self.codeSelectedMenu, pageNumber: self.page, pageSize: 15, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                            (data: Array<Task>, success: Bool, message: String) in
                            if (success) {
                                DispatchQueue.main.async{
                                    self.activityIndicator.stopAnimating()
                                    self.myList += data
                                    DispatchQueue.main.async{
                                        self.TableView.reloadData()
                                    }
                                    print (self.page)
                                    if data.count > 0 {
                                        self.page += 1
                                    }
                                    else {
                                        self.scrolledToEnd = true
                                    }
                                }
                                self.callbackComplete = true
                            } else {
                                print(message)
                            }
                        })
                    } else {
                        self.restClient.getTasks(method: self.selectedMenu, pageNumber: self.page, pageSize: 15, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                            (data: Array<Task>, success: Bool, message: String) in
                            if (success) {
                                DispatchQueue.main.async{
                                    self.activityIndicator.stopAnimating()
                                    self.myList += data
                                    DispatchQueue.main.async{
                                        self.TableView.reloadData()
                                    }
                                    print (self.page)
                                    if data.count > 0 {
                                        self.page += 1
                                    }
                                    else {
                                        self.scrolledToEnd = true
                                    }
                                }
                                self.callbackComplete = true
                            } else {
                                print(message)
                            }
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func onSort(_ sender: Any) {
        let utility = Utility()
        let alert = UIAlertController(title: "Sort", message: "Scegli un criterio di ordinamento", preferredStyle: UIAlertControllerStyle.alert)
        if storeUtil.getLastMenu() != "Done" {
            alert.addAction(UIAlertAction(title: "Priority A -> Z", style: .default, handler: { action in
                self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
                self.SortButton.title = action.title
            }))
            alert.addAction(UIAlertAction(title: "Priority Z -> A", style: .default, handler: { action in
                self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
                self.SortButton.title = action.title
            }))
        }
        alert.addAction(UIAlertAction(title: "Name A -> Z", style: .default, handler: { action in
            self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
            self.SortButton.title = action.title
        }))
        alert.addAction(UIAlertAction(title: "Name Z -> A", style: .default, handler: { action in
            self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
            self.SortButton.title = action.title
        }))
        if storeUtil.getLastMenu() != "Done" {
            alert.addAction(UIAlertAction(title: "Due date A -> Z", style: .default, handler: { action in
                self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
                self.SortButton.title = action.title
            }))
            alert.addAction(UIAlertAction(title: "Due date Z -> A", style: .default, handler: { action in
                self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
                self.SortButton.title = action.title
            }))
        }
        else {
            alert.addAction(UIAlertAction(title: "Performed date A -> Z", style: .default, handler: { action in
                self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
                self.SortButton.title = action.title
            }))
            alert.addAction(UIAlertAction(title: "Performed date Z -> A", style: .default, handler: { action in
                self.performeSort(sort: utility.getSortString(selectedSort: action.title!))
                self.SortButton.title = action.title
            }))
        }
        
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func performeSort(sort: String) {
        var filter = self.searchField.text
        if filter == "" {
            filter = nil
        }
        else {
            filter = "&s=" + (filter?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!
        }
        if self.utility.checkMenuStatic(menuSelected: self.selectedMenu) {
            self.restClient.getWFTask(WFCode: self.codeSelectedMenu, pageNumber: 0, pageSize: self.myList.count < 15 ? 15 : self.myList.count, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                (data: Array<Task>, success: Bool, message: String) in
                if (success) {
                    self.myList = data
                    DispatchQueue.main.async{
                        self.TableView.reloadData()
                    }
                } else {
                    print(message)
                }
            })
        } else {
            self.restClient.getTasks(method: self.selectedMenu, pageNumber: 0, pageSize: self.myList.count < 15 ? 15: self.myList.count, userId: self.mySession.user_id, filter: filter, sort: sort, callback: {
                (data: Array<Task>, success: Bool, message: String) in
                if (success) {
                    self.myList = data
                    DispatchQueue.main.async{
                        self.TableView.reloadData()
                    }
                } else {
                    print(message)
                }
            })
        }
        self.scrolledToEnd = false
    }
    
    
}
