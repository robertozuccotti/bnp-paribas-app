//
//  ViewController.swift
//  Prova
//
//  Created by Interlem on 22/12/16.
//  Copyright © 2016 Interlem. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton!
    
    let storeUtil = StoreUtil()
    let me = self
    var companies: Array<String> = []
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view, typically from a nib.
        self.picker.delegate = self
        self.picker.dataSource = self
        
        self.companies = self.storeUtil.getCompanies()
        let userInfo = self.storeUtil.getLoggedUserInfo()
        self.username.becomeFirstResponder()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return companies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return companies[row]
    }
    
    
    @IBAction func onLogin(_ sender: UIButton) {
        self.performLogin()
    }
    
    @IBAction func typeUsernameEnd(_ sender: Any) {
        self.password.becomeFirstResponder()
    }
    @IBAction func typePasswordEnd(_ sender: Any) {
        self.view.endEditing(true)
        self.performLogin()
    }
    
    func performLogin() {
        let companyId = picker.selectedRow(inComponent: 0)
        let myUser = self.username.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        let myPassword = self.password.text
        let user = User()
        user.username = myUser!
        user.password = myPassword!
        user.company = self.companies[companyId]
        
        let me = self
        let nextViewController = me.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController")
        
        let myClient = RestClient()
        
        let myColor : UIColor = UIColor(white: 1, alpha: 0.3)
        self.username.layer.borderColor = myColor.cgColor
        self.username.layer.borderWidth = 1.0
        self.username.layer.cornerRadius = 5.0
        self.password.layer.borderColor = myColor.cgColor
        self.password.layer.borderWidth = 1.0
        self.password.layer.cornerRadius = 5.0
        
        if myUser == "" {
            OperationQueue.main.addOperation {
                let myColor : UIColor = UIColor.red
                self.username.layer.borderColor = myColor.cgColor
                self.username.layer.borderWidth = 1.0
                self.username.layer.cornerRadius = 5.0
            }
        }
        if myPassword == "" {
            OperationQueue.main.addOperation {
                let myColor : UIColor = UIColor.red
                self.password.layer.borderColor = myColor.cgColor
                self.password.layer.borderWidth = 1.0
                self.password.layer.cornerRadius = 5.0
            }
        }
        
        if myPassword != "" && myUser != "" {
            
            var overlay : UIView?
            overlay = UIView(frame: view.frame)
            overlay!.backgroundColor = UIColor.black
            overlay!.alpha = 0.6
            
            view.addSubview(overlay!)
            
            
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
            activityIndicator.color = UIColor.white
            activityIndicator.center.x = view.center.x
            activityIndicator.center.y = view.center.y
            view.addSubview(activityIndicator)
            
            DispatchQueue.main.async{
                self.activityIndicator.startAnimating()
            }
            self.login.isEnabled = false
            myClient.login(user: user, callback: {(success: Bool, message: String) in
                if success {
                    self.storeUtil.setLoggedUserInfo(username: myUser!, password: myPassword!, company: user.company)
                    self.storeUtil.setLastMenu(menu: "To do")
                    OperationQueue.main.addOperation {
                        me.present(nextViewController!, animated:true, completion:nil)
                    }
                } else {
                    OperationQueue.main.addOperation {
                        let alert = UIAlertController(title: "Attenzione", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        let retryAction = UIAlertAction(title: "Riprova", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
                            overlay!.removeFromSuperview()
                        })
                        alert.addAction(retryAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                self.login.isEnabled = true
            })
        }
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
