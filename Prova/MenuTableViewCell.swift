//
//  MenuTableViewCell.swift
//  Prova
//
//  Created by Interlem on 18/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSpace: UILabel!
    @IBOutlet weak var lblMenuName: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
