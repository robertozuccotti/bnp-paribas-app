//
//  MenuViewController.swift
//  Prova
//
//  Created by Interlem on 18/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
}

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var menuNameArr:Array = [String]()
    var iconeImage:Array = [UIImage]()
    var company = String()
    let storeUtil = StoreUtil()
    let restClient = RestClient()
    @IBOutlet weak var userLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuNameArr = ["To do","My tasks","Available tasks","Hidden","Done","","Apps"]
        iconeImage = [UIImage(named: "icon_todo")!,UIImage(named: "icon_mytask")!,UIImage(named: "icon_available")!,UIImage(named: "icon_hidden")!,UIImage(named: "icon_done")!,UIImage(),UIImage()]
        
        let company = self.storeUtil.getLoggedUserInfo()
        
        let companyWF = self.storeUtil.getCompanyWFNames(company: company.company)
        
        menuNameArr += companyWF
        for index in 0...companyWF.count-1 {
            iconeImage += [UIImage(named: "ic_panorama_fish_eye_white")!]
        }
        
        menuNameArr += ["", "Crediti", "Logout"]
        iconeImage += [UIImage(),UIImage(named: "ic_info_outline_white")!,UIImage(named: "ic_exit_to_app_white")!]
        
        if self.userLabel.text == "" {
            self.restClient.getUserSession { (data: Session, success: Bool, message: String) in
                self.restClient.getCompleteName(userId: data.user_id, callback: {
                    (data: String, success: Bool, message: String) in
                    if (success) {
                        self.userLabel.text = "Welcome: " + data
                    }
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
        cell.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        cell.imgIcon.image = iconeImage[indexPath.row]
        cell.lblMenuName.text! = menuNameArr[indexPath.row]
        
        if cell.lblMenuName.text! == "" {
            cell.isUserInteractionEnabled = false
        }
        
        if cell.lblMenuName.text! == "Apps" {
            cell.isUserInteractionEnabled = false
            
            let mySize = CGSize(width: 0, height: 0 )
            cell.imgIcon.frame.size = mySize
            
            cell.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        }
        
        if cell.lblMenuName.text! == "Available tasks" || cell.lblMenuName.text! == "My tasks" {
            
            let horizonalContraints = NSLayoutConstraint(item: cell.lblSpace, attribute: .leftMargin, relatedBy: .equal, toItem: cell, attribute: .leftMargin, multiplier: 1.0, constant: 30)
            
            NSLayoutConstraint.activate([horizonalContraints])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let revealviewcontroller:SWRevealViewController = self.revealViewController()
        let cell:MenuTableViewCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        
        if cell.lblMenuName.text == "Logout" {
            let me = self
            let nextViewController = me.storyboard?.instantiateViewController(withIdentifier: "loginView")
            
            let alert = UIAlertController(title: "Logout", message: "Sei sicuro di effettuare il logout?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                let myClient = RestClient()
                myClient.logout(callback: {(success: Bool, message: String) in
                    if (success) {
                        let storeUtil = StoreUtil()
                        storeUtil.deleteLoggedUserInfo()
                        
                        OperationQueue.main.addOperation{
                            me.present(nextViewController!, animated:true, completion:nil)
                        }
                    }
                    else {
                        //TODO: gestire il messaggio di errore (popup / toast)
                    }
                })
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "loginView")
                self.present(nextViewController!, animated:true, completion:nil)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else if cell.lblMenuName.text == "Crediti" {
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "CreditsViewController") as! CreditsViewController
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            
            revealViewController().pushFrontViewController(newFrontViewController, animated: true)
        }
        else {
            self.storeUtil.setLastMenu(menu: cell.lblMenuName.text!)
            
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desController = mainStoryboard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
            let newFrontViewController = UINavigationController.init(rootViewController:desController)
            
            revealViewController().pushFrontViewController(newFrontViewController, animated: true)
        }
    }
}
