//
//  Process.swift
//  Prova
//
//  Created by Interlem on 16/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class Process {
    
    var id: String
    var end_date: String
    var startedBySubstitute: String
    var start: String
    var state: String
    var rootCaseId: String
    var started_by: String
    var processDefinitionId: String
    var last_update_date: String
    
    init() {
        
        self.id = ""
        self.end_date = ""
        self.startedBySubstitute = ""
        self.start = ""
        self.state = ""
        self.rootCaseId = ""
        self.started_by = ""
        self.processDefinitionId = ""
        self.last_update_date = ""
        
    }
    
    
}
