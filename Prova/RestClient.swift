//
//  RestClient.swift
//  Prova
//
//  Created by Interlem on 11/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class RestClient {

    func login(user: User, callback : @escaping (Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let reach = Reach()
        let config = restUtil.getConfigs()
        if (config != nil) {
            let baseURL = config?["Base URL"] as! String
            let serviceURL = config?["Login URL"] as! String
            let params = "?username="+user.username+"&password="+user.password+"&redirect=false"
            let completeURL = URL(string: baseURL+serviceURL+params)
            if completeURL != nil {
                let request = NSMutableURLRequest(url:completeURL!)
                request.httpMethod = "POST"
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    data, response, error in
                    if error != nil {
                        if reach.connectionStatus().description == "Offline" {
                            callback(false, "Connessione Internet assente")
                        }
                        else {
                            callback(false, "Impossibile effettuare il login")
                        }
                        print("error=\(error)")
                    }
                    else {
                        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                            print("statusCode should be 200, response = \(response)")
                            callback(false, "Credenziali errate")
                        }
                        else {
                            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                            callback(true, responseString as! String)
                        }
                    }
                }
                task.resume()
            }
            else {
                callback(false, "Credenziali errate")
            }
        }
    }
    
    func logout(callback : @escaping (Bool, String) -> ()) {
    
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        if (config != nil) {
            let baseURL = config?["Base URL"] as! String
            let serviceURL = config?["Logout URL"] as! String
            let params = "?redirect=false"
            let completeURL = URL(string: baseURL+serviceURL+params)
            let request = NSMutableURLRequest(url:completeURL!)
            request.httpMethod = "POST"
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                if error != nil {
                    print("error=\(error)")
                    callback(false, "Impossibile effettuare il logout")
                }
                else {
                    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                        print("statusCode should be 200, response = \(response)")
                        callback(false, "Impossibile effettuare il logout")
                    }
                    else {
                        let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        callback(true, responseString as! String)
                    }
                }
            }
            task.resume()
        }
    }
    
    func getUserSession(callback : @escaping (Session, Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let session = Session()
        let baseURL = config?.value(forKey: "Base URL") as! String
        let serviceURL = config?.value(forKey: "User Session URL") as! String
        let completeURL = URL(string: baseURL+serviceURL)
        let request = NSMutableURLRequest(url:completeURL!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil {
                print("error=\(error)")
                callback(session, false, "Impossibile recuperare i dati")
                return
            }
            else {
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                let jsonObj: Any
                do {
                    jsonObj = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions())
                    let dict = jsonObj as? [String: AnyObject]
                    session.user_name = dict?["user_name"] as! String
                    session.session_id = dict?["session_id"] as! String
                    session.is_technical_user = dict?["is_technical_user"] as! String
                    session.user_id = dict?["user_id"] as! String
                    session.conf = dict?["conf"] as! String
                    session.version = dict?["version"] as! String
                } catch {
                    print(error)
                }
                callback(session, true, responseString as! String)
            }
        }
        task.resume()
    }
    
    func getCompleteName(userId: String, callback: @escaping (String, Bool, String) -> ()){
    
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let session = Session()
        let baseURL = config?.value(forKey: "Base URL") as! String
        let serviceURL = config?.value(forKey: "User URL") as! String
        let completeURL = URL(string: baseURL+serviceURL+"/"+userId)
        let request = NSMutableURLRequest(url:completeURL!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil {
                print("error=\(error)")
                callback("", false, "Impossibile recuperare i dati")
                return
            }
            else {
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                let jsonObj: Any
                var completeName = ""
                do {
                    jsonObj = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions())
                    let dict = jsonObj as? [String: AnyObject]
                    let firstName = dict?["firstname"] as! String
                    let lastName = dict?["lastname"] as! String
                    completeName = firstName + " " + lastName
                } catch {
                    print(error)
                }
                callback(completeName, true, responseString as! String)
            }
        }
        task.resume()
        
    }
    
    func getTasks(method: String, pageNumber: Int, pageSize: Int, userId: String, filter: String? = nil, sort: String? = nil, callback : @escaping (Array<Task>, Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?.value(forKey: "Base URL") as! String
        var serviceURL = config?.value(forKey: "List Pending Task URL") as! String
        var params = ""
        switch method {
            case "To do":
                params = "?p="+String(pageNumber)+"&c="+String(pageSize)+"&f=state%3dready&f=user_id%3d"+userId+"&d=rootContainerId"
                break
            
            case "My tasks":
                params = "?p="+String(pageNumber)+"&c="+String(pageSize)+"&f=user_id%3d"+userId+"&f=assigned_id%3d"+String(userId)+"&d=rootContainerId"
                break
            
            case "Available tasks":
                params = "?p="+String(pageNumber)+"&c="+String(pageSize)+"&f=state%3dready&f=user_id%3d"+userId+"&f=assigned_id%3d0&d=rootContainerId"
                break
            
            case "Hidden":
                params = "?p="+String(pageNumber)+"&c="+String(pageSize)+"&f=state%3dready&f=user_id%3d"+String(userId)+"&f=hidden_user_id%3d"+userId+"&d=rootContainerId"
                break
            
            case "Done":
                params = "?p="+String(pageNumber)+"&c="+String(pageSize)+"&f=assigned_id%3d"+userId+"&d=rootContainerId"
                serviceURL = config?["List Archived Task URL"] as! String
                break
            
            default: break
        }

        if sort != nil {
            params += sort!
        }
        if filter != nil {
            params += filter!
        }
        let completeURL = baseURL+serviceURL+params
        restUtil.execRequestTask(url: completeURL, type: "GET", filter: filter, callback: {(data: Array<Task>, success: Bool, message: String) in
            callback(data, success, message)
        })
    
    }
    
    func getWFTask(WFCode: String, pageNumber: Int, pageSize: Int, userId: String, filter: String? = nil, sort: String? = nil, callback : @escaping (Array<Task>, Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?["Base URL"] as! String
        let serviceURL = config?["List Pending Task URL"] as! String
        var params = "?p="+String(pageNumber)+"&c="+String(pageSize)+"&f=user_id%3d"+userId+"&f=processId%3d"+WFCode+"&f=state%3dready&d=rootContainerId"
        if sort != nil {
            params += sort!
        }
        if filter != nil {
            params += filter!
        }
        let completeURL = baseURL+serviceURL+params
        restUtil.execRequestTask(url: completeURL, type: "GET", filter: filter, callback: {(data: Array<Task>, success: Bool, message: String) in
            callback(data, success, message)
        })
        
    }
    
    func assignTaskToUser(taskId: String, userId: String, callback : @escaping (Array<Task>, Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?["Base URL"] as! String
        let serviceURL = config?["List Pending Task URL"] as! String + "/" + taskId
        let completeURL = baseURL+serviceURL
        var myPayload = [String: Any]()
        myPayload["assigned_id"] = String(userId)
        let jsonData = try? JSONSerialization.data(withJSONObject: myPayload)
        restUtil.execRequestTask(url: completeURL, type: "PUT", payload: jsonData, callback: {(data: Array<Task>, success: Bool, message: String) in
            callback(data, success, message)
        })
        
    }
    
    func executeTask(taskId: String, myPayload: [String:Any], callback : @escaping (Array<Task>, Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?["Base URL"] as! String
        let serviceURL = config?["List Activity URL"] as! String + "/" + taskId
        let completeURL = baseURL+serviceURL
        let jsonData = try? JSONSerialization.data(withJSONObject: myPayload)
        restUtil.execRequestTask(url: completeURL, type: "PUT", payload: jsonData, callback: {(data: Array<Task>, success: Bool, message: String) in
            callback(data, success, message)
        })
        
    }
    
    func hideTask(taskId: String, userId: String, callback : @escaping (Array<Task>, Bool, String) -> ())
    {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?["Base URL"] as! String
        let serviceURL = config?["List Hidden Task URL"] as! String + "/"
        let completeURL = baseURL+serviceURL
        var myPayload = [String: Any]()
        myPayload["user_id"] = userId
        myPayload["task_id"] = taskId
        let jsonData = try? JSONSerialization.data(withJSONObject: myPayload)
        restUtil.execRequestTask(url: completeURL, type: "POST", payload: jsonData, callback: {(data: Array<Task>, success: Bool, message: String) in
            callback(data, success, message)
        })
        
    }
    
    func retrieveTask(taskId: String, userId: String, callback : @escaping (Array<Task>, Bool, String) -> ())
    {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?["Base URL"] as! String
        let serviceURL = config?["List Hidden Task URL"] as! String + "/"+userId+"/"+taskId
        let completeURL = baseURL+serviceURL
        var myPayload = [String: Any]()
        myPayload["user_id"] = userId
        myPayload["task_id"] = taskId
        let jsonData = try? JSONSerialization.data(withJSONObject: myPayload)
        restUtil.execRequestTask(url: completeURL, type: "DELETE", payload: jsonData, callback: {(data: Array<Task>, success: Bool, message: String) in
            callback(data, success, message)
        })
        
    }
    
    func getCaseVariables(caseId: String, callback : @escaping (Case, Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?["Base URL"] as! String
        let serviceURL = config?["List Case Variables URL"] as! String
        let params = "?p=0&c=1000&f=case_id%3d"+String(caseId)
        let completeURL = baseURL+serviceURL+params
        restUtil.execRequestCaseVariables(url: completeURL, type: "GET", callback: {(data: [String: String], success: Bool, message: String) in
            
            self.getCaseDocument(caseId: caseId, callback: { (document:[Document], success: Bool, message: String) in
                let caseModel = Case()
                caseModel._data = data
                caseModel._document = document
                
                callback(caseModel, success, message)
            })
        })
        
    }
    
    func getCaseDocument(caseId: String, callback : @escaping ([Document], Bool, String) -> ()) {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let baseURL = config?["Base URL"] as! String
        let serviceURL = config?["List Case Documents URL"] as! String
        let params = "?p=0&c=-1&f=caseId%3d"+String(caseId)
        let completeURL = baseURL+serviceURL+params
        restUtil.execRequestCaseDocuments(url: completeURL, type: "GET", callback: {(data: [Document], success: Bool, message: String) in
            callback(data, success, message)
        })
        
    }
}
