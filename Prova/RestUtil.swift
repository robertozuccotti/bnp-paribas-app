//
//  RestUtil.swift
//  Prova
//
//  Created by Interlem on 11/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class RestUtil {
    
    func execRequestTask(url: String, type: String, payload: Data? = nil, filter: String? = nil, callback : @escaping (Array<Task>, Bool, String) -> ()) {
    
        let completeURL = URL(string: url)
        if completeURL != nil {
            let request = NSMutableURLRequest(url:completeURL!)
            request.httpMethod = type
            if payload != nil {
                request.httpBody = payload
            }
            request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            request.setValue("application/json", forHTTPHeaderField:"Accept")
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                if error != nil {
                    callback([], false, "Impossibile recuperare i dati")
                    return
                }
                else {
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    var jsonList: Array<Any>!
                    var taskList = [Task]()
                    do {
                        jsonList = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? Array
                        if jsonList == nil {
                            callback([], true, "Non sono presenti dati")
                            return
                        }
                    } catch {
                        print(error)
                        callback([], true, "Non sono presenti dati")
                        return
                    }
                    if type == "GET" {
                        for item in jsonList {
                            if let dict = item as? [String: AnyObject] {
                                let task = Task()
                                for (key, value) in dict {
                                    print(key)
                                    task.setValue(String(describing: value), forKey: "_"+key)
                                }
                                let storeUtil = StoreUtil()
                                let WFName = storeUtil.getWFName(company: storeUtil.getLoggedUserInfo().company, WF: task._processId)
                                task._processName = WFName
                                taskList.append(task)
                            }
                        }
                    }
                    callback(taskList, true, responseString as! String)
                    return
                }
            }
            task.resume()
        }
    }
    
    func execRequestCaseVariables(url: String, type: String, payload: Data? = nil, filter: String? = nil, callback : @escaping ([String: String], Bool, String) -> ()) {
        
        var myDict = [String: String]()
        let completeURL = URL(string: url)
        if completeURL != nil {
            let request = NSMutableURLRequest(url:completeURL!)
            request.httpMethod = type
            if payload != nil {
                request.httpBody = payload
            }
            request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            request.setValue("application/json", forHTTPHeaderField:"Accept")
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                if error != nil {
                    callback(myDict, false, "Impossibile (recuperare) i dati")
                    return
                }
                else {
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    var jsonList: Array<Any>!
                    do {
                        jsonList = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? Array
                        if jsonList == nil {
                            callback(myDict, true, "Non (sono) presenti dati")
                            return
                        }
                    } catch {
                        print(error)
                    }
                    if type == "GET" {
                        for item in jsonList {
                            if let dict = item as? [String: AnyObject] {
                                let myKey = dict["name"] as? String
                                myDict[myKey!] = dict["value"] as? String != "null" ? dict["value"] as? String : ""
                            }
                        }
                    }
                    callback(myDict, true, responseString as! String)
                    return
                }
            }
            task.resume()
        }
    }
    
    func execRequestCaseDocuments(url: String, type: String, payload: Data? = nil, filter: String? = nil, callback : @escaping ([Document], Bool, String) -> ()) {
        
        let completeURL = URL(string: url)
        if completeURL != nil {
            let request = NSMutableURLRequest(url:completeURL!)
            request.httpMethod = type
            if payload != nil {
                request.httpBody = payload
            }
            request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            request.setValue("application/json", forHTTPHeaderField:"Accept")
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                if error != nil {
                    callback([], false, "Impossibile recuperare i dati")
                }
                else {
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    var jsonList: Array<Any>!
                    var documentList = [Document]()
                    do {
                        jsonList = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? Array
                        if jsonList == nil {
                            callback([], true, "Non sono presenti dati")
                        }
                    } catch {
                        print(error)
                    }
                    if type == "GET" {
                        let restUtil = RestUtil()
                        let config = restUtil.getConfigs()
                        let baseURL = config?["Base URL"] as! String
                        for item in jsonList {
                            if let dict = item as? [String: AnyObject] {
                                let document = Document()
                                for (key, value) in dict {
                                    print("Document: "+key)
                                    document.setValue(String(describing: value), forKey: "_"+key)
                                }
                                if document._url != "" {
                                    var splittedString = document._url.components(separatedBy: "?")
                                    let correctUrl = baseURL + "/portal/documentDownload?" + splittedString[1]
                                    document._url = correctUrl
                                }
                                documentList.append(document)
                            }
                        }
                    }
                    callback(documentList, true, responseString as! String)
                }
            }
            task.resume()
        }
    }
    
    func getConfigs() -> NSDictionary?
    {
        
        let config: NSDictionary
        if let path = Bundle.main.path(forResource: "Config", ofType: "plist") {
            config = NSDictionary(contentsOfFile: path)!
            return config
        }
        return nil
    }
    
    func setCookies(response: URLResponse) {
        
        if let httpResponse = response as? HTTPURLResponse {
            if let headerFields = httpResponse.allHeaderFields as? [String: String] {
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: response.url!)
            }
        }
    }

}
