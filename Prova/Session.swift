//
//  Session.swift
//  Prova
//
//  Created by Interlem on 16/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class Session {
    
    var user_name: String
    var session_id: String
    var is_technical_user: String
    var user_id: String
    var conf: String
    var version: String
    
    init() {
        
        self.user_name = ""
        self.session_id = ""
        self.is_technical_user = ""
        self.user_id = ""
        self.conf = ""
        self.version = ""
        
    }
    
}
