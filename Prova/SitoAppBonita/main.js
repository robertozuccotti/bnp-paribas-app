var x2js = new X2JS();

function convertXml2JSon(xml) {
    var json = x2js.xml_str2json(xml);
	return json;
}

$(document).ready(function(){
	readTextFile();
});

function readTextFile()
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", "manifest.plist", true);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            var allText = rawFile.responseText;
			allXML = allText.split('.dtd">\n')[1];
			var json = convertXml2JSon(allXML);
			$("#version").text(json.plist._version);
        }
    }

    rawFile.send();
}