//
//  StandardTableViewCell.swift
//  Prova
//
//  Created by Interlem on 08/02/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

class StandardTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textfield: UITextField!
    
    func setupWithModel(model: DetailViewCellModel) {
        if model.required {
            label.text = model.label + " *"
            label.textColor = UIColor.red
        } else {
           label.text = model.label
        }
        
        if model.editable {
            textfield.isEnabled = true
        } else {
            textfield.isEnabled = false
        }
        
        textfield.text = model.text
        self.selectionStyle = .none
    }
}
