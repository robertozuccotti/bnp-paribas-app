//
//  StoreUtil.swift
//  Prova
//
//  Created by Interlem on 13/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class StoreUtil {

    func setLoggedUserInfo(username: String, password: String, company: String) {
    
        let defaults = UserDefaults.standard
        defaults.setValue(username, forKey: "username")
        defaults.setValue(password, forKey: "password")
        defaults.setValue(company, forKey: "company")
    }
    
    func deleteLoggedUserInfo() {
    
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "username")
        defaults.removeObject(forKey: "password")
        defaults.removeObject(forKey: "company")
    
    }
    
    func getLoggedUserInfo() -> (username: String, password: String, company:String) {
    
        let defaults = UserDefaults.standard
        var username = defaults.string(forKey: "username")
        if username == nil {
            username = ""
        }
        var password =  defaults.string(forKey: "password")
        if password == nil {
            password = ""
        }
        var company = defaults.string(forKey: "company")
        if company == nil {
            company = ""
        }

        return (username!, password!, company!)
        
    }
    
    func setLastMenu(menu: String) {
        
        let defaults = UserDefaults.standard
        defaults.set(menu, forKey: "menu")
        
    }
    
    func getLastMenu() -> String {
        
        let defaults = UserDefaults.standard
        var menu = defaults.string(forKey: "menu")
        if menu == nil {
            menu = ""
        }
        return menu!
        
    }
    
    func userIsLogged() -> Bool {
    
        let defaults = getLoggedUserInfo()
        if (defaults.username == "" || defaults.password == "" || defaults.company == "") {
            return false
        }
        else {
            return true
        }
    
    }
    
    func getCompanies() -> Array<String> {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let companies = config?["Companies"] as! Array<NSObject>
        var companyList: Array<String> = []
        for c in companies {
            let obj = c.value(forKey: "name") as! String
            companyList.append(obj)
        }
        return companyList
    }
    
    func getCompanyWFNames(company: String) -> Array<String> {
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let companiesStruct = config?["Companies"] as! Array<NSObject>
        var WFList: Array<String> = []
        let companies = self.getCompanies()
        let index = companies.index(of: company)
        let myCompany = companiesStruct[index!]
        let WFStruct = myCompany.value(forKey: "workflow") as! Array<NSObject>
        for wf in WFStruct {
            let obj = wf.value(forKey: "name") as! String
            WFList.append(obj)
            
        }
        return WFList
        
    }
    
    func getCompanyWFCodes(company: String) -> Array<String> {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let companiesStruct = config?["Companies"] as! Array<NSObject>
        var WFList: Array<String> = []
        let companies = self.getCompanies()
        let index = companies.index(of: company)
        let myCompany = companiesStruct[index!]
        let WFStruct = myCompany.value(forKey: "workflow") as! Array<NSObject>
        for wf in WFStruct {
            let obj = wf.value(forKey: "code") as! String
            WFList.append(obj)
            
        }
        return WFList
        
    }
    
    func getWFCode(company: String, WF: String) -> String {
    
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let companies = self.getCompanies()
        let WFs = self.getCompanyWFNames(company: company)
        let companyIndex = companies.index(of: company)
        let WFIndex = WFs.index(of: WF)
        let companiesStruct = config?["Companies"] as! Array<NSObject>
        let WFStruct = companiesStruct[companyIndex!].value(forKey: "workflow") as! Array<NSObject>
        let myCode = WFStruct[WFIndex!].value(forKey: "code") as! String
        return myCode
        
    }
    
    func getWFName(company: String, WF: String) -> String {
        
        let restUtil = RestUtil()
        let config = restUtil.getConfigs()
        let companies = self.getCompanies()
        let WFs = self.getCompanyWFCodes(company: company)
        let companyIndex = companies.index(of: company)
        let WFIndex = WFs.index(of: WF)
        if WFIndex != nil {
            let companiesStruct = config?["Companies"] as! Array<NSObject>
            let WFStruct = companiesStruct[companyIndex!].value(forKey: "workflow") as! Array<NSObject>
            let myName = WFStruct[WFIndex!].value(forKey: "name") as! String
            return myName
        }
        else {
            return ""
        }
    }
    
    func setSelectedTask(task: Task) {
        
        let defaults = UserDefaults.standard
        defaults.set(task._caseId, forKey: "task")
        defaults.set(task._name, forKey: "fase")
        defaults.set(task._id, forKey: "id")
        
    }
    
    func getSelectedTask() -> Task {
        
        let defaults = UserDefaults.standard
        let myTask = Task()
        var task = defaults.string(forKey: "task")
        var fase = defaults.string(forKey: "fase")
        if task == nil {
            task = ""
        }
        if fase == nil {
            fase = ""
        }
        let id = defaults.string(forKey: "id")
        myTask._caseId = task!
        myTask._name = fase!
        myTask._id = id!
        return myTask
        
    }
    
}
