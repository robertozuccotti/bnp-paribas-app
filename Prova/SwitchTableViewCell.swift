//
//  SwitchTableViewCell.swift
//  Prova
//
//  Created by Interlem on 08/02/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

protocol SwitchViewCellDelegate {
    func didTappedSwitch(cell: SwitchTableViewCell)
}

class SwitchTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var `switch`: UISwitch!
    
    var delegate: SwitchViewCellDelegate!
    
    @IBAction func switchValueChanged(_ sender: Any) {
        delegate.didTappedSwitch(cell: self)
    }
    
    func setupWithModel(model: DetailViewCellModel) {
        if model.required {
            label.text = model.label + " *"
            label.textColor = UIColor.red
        } else {
            label.text = model.label
        }
        
        if model.editable {
            `switch`.isEnabled = true
        } else {
            `switch`.isEnabled = false
        }
        
        `switch`.setOn(model.approved, animated: false)
        self.selectionStyle = .none
    }
}
