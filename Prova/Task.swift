//
//  Task.swift
//  Prova
//
//  Created by Interlem on 16/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class Task: NSObject {
    
    var _displayDescription: String
    var _executedBySubstitute: String
    var _processId: String
    var _processName: String
    var _state: String
    var _rootContainerId: String
    var _rootCaseId: String
    var _type: String
    var _assigned_id: String
    var _assigned_date: String
    var _id: String
    var _executedBy: String
    var _caseId: String
    var _priority: String
    var _actorId: String
    var _description: String
    var _name: String
    var _reached_state_date: String
    var _displayName: String
    var _dueDate: String
    var _last_update_date: String
    var _archivedDate: String
    var _sourceObjectId: String
    
    var _parentCaseId: String
    
    override required init() {
        
        self._displayDescription = ""
        self._executedBySubstitute = ""
        self._processId = ""
        self._processName = ""
        self._state = ""
        self._rootContainerId = ""
        self._rootCaseId = ""
        self._type = ""
        self._assigned_id = ""
        self._assigned_date = ""
        self._id = ""
        self._executedBy = ""
        self._caseId = ""
        self._priority = ""
        self._actorId = ""
        self._description = ""
        self._name = ""
        self._reached_state_date = ""
        self._displayName = ""
        self._dueDate = ""
        self._last_update_date = ""
        self._archivedDate = ""
        self._sourceObjectId = ""
     
        self._parentCaseId = ""
    }
    
}
