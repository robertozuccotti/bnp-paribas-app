//
//  TextAreaTableViewCell.swift
//  Prova
//
//  Created by Interlem on 08/02/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import UIKit

class TextAreaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    func setupWithModel(model: DetailViewCellModel) {
        if model.required {
            label.text = model.label + " *"
            label.textColor = UIColor.red
        } else {
            label.text = model.label
        }
        
        if model.editable {
            textView.isEditable = true
        } else {
            textView.isEditable = false
            textView.layer.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1).cgColor
        }
        
        textView.text = model.text
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UITableView().separatorColor?.cgColor
        textView.layer.cornerRadius = 5
        self.selectionStyle = .none
    }
}
