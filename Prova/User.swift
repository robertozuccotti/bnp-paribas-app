//
//  LoginModel.swift
//  Prova
//
//  Created by Interlem on 13/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class User {

    var username: String
    var password: String
    var company: String
    var jsessionId: String
    
    init() {
        
        self.username = ""
        self.password = ""
        self.company = ""
        self.jsessionId = ""
        
    }
    
}
