//
//  utility.swift
//  Prova
//
//  Created by Interlem on 25/01/17.
//  Copyright © 2017 Interlem. All rights reserved.
//

import Foundation

class Utility {
    func checkMenuStatic (menuSelected: String) -> Bool {
        let menuLabel = ["To do", "My tasks", "Available tasks", "Hidden", "Done"]
        
        if menuLabel.index(of: menuSelected) == nil {
            return true
        } else {
            return false
        }
    }
    
    func getSortString (selectedSort: String) -> String {
        let sortLabels = ["Priority A -> Z", "Priority Z -> A", "Name A -> Z", "Name Z -> A", "Due date A -> Z", "Due date Z -> A", "Performed date A -> Z", "Performed date Z -> A"]
        let sortKeys = ["&o=priority%20ASC", "&o=priority%20DESC", "&o=displayName%20ASC", "&o=displayName%20DESC", "&o=dueDate%20ASC", "&o=dueDate%20DESC", "&o=reached_state_date%20ASC", "&o=reached_state_date%20DESC"]
        let index = sortLabels.index(of: selectedSort)
        return sortKeys[index!]
    }
}
