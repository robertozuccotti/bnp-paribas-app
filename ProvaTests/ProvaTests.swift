//
//  ProvaTests.swift
//  ProvaTests
//
//  Created by Interlem on 22/12/16.
//  Copyright © 2016 Interlem. All rights reserved.
//

import XCTest
@testable import Prova

class ProvaTests: XCTestCase {
    
    let restClient = RestClient()
    let restUtil = RestUtil()
    let storeUtil = StoreUtil()
    let user = User()
    let page = 0
    let numberPerPage = 500
    let userId = 2601
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.user.username = "lucas"
        self.user.password = "lucas"
        self.user.company = 0
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLogin() {
        
        let myExpectation = expectation(description: "dati username e password viene effettuato il log in chiamando bonita")
        
        restClient.login(user: self.user, callback: {(success: Bool, message: String) in
            XCTAssertTrue(success)
            
            myExpectation.fulfill()
        })
        
        waitForExpectations(timeout: 60) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
        
    }
    
    func testGetCompanies() {
    
        let lista = storeUtil.getCompanies()
        if lista.count == 0 {
            XCTFail("waitForExpectationsWithTimeout errored")
        }
        
        
    }
    
    func testGetCompaniesWF() {
        
        let lista = storeUtil.getCompanyWF(company: "CIB")
        if lista.count == 0 {
            XCTFail("waitForExpectationsWithTimeout errored")
        }
        
    }
    
    func testGetWFCode() {
        
        let lista = storeUtil.getWFCode(company: "CIB", WF: "Rda Manutenzione Ordinaria 1.0")
        if lista == "" {
            XCTFail("waitForExpectationsWithTimeout errored")
        }
        
    }
    
    
//    func testListOpenedProcessIntances() {
//        
//        let myExpectation = expectation(description: "dopo aver effettuato il login, viene restituita la lista dei processi aperti")
//        
//        restClient.listOpenedProcessIntances(page: self.page, numberPerPage: self.numberPerPage, callback: {(success: Bool, message: String) in
//            XCTAssertTrue(success)
//            
//            myExpectation.fulfill()
//        })
//        
//        waitForExpectations(timeout: 60) { error in
//            if let error = error {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//        
//    }
    
//    func testListArchivedProcessIntances() {
//        
//        let myExpectation = expectation(description: "dopo aver effettuato il login, viene restituita la lista dei processi archiviati")
//        
//        restClient.listArchivedProcessIntances(page: self.page, numberPerPage: self.numberPerPage, callback: {(success: Bool, message: String) in
//            XCTAssertTrue(success)
//            
//            myExpectation.fulfill()
//        })
//        
//        waitForExpectations(timeout: 60) { error in
//            if let error = error {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//        
//    }
//    
//    func testGetUserSession() {
//    
//        let myExpectation = expectation(description: "dopo aver effettuato il login, viene restituita la sessione aperta dall'utente")
//        
//        restClient.userSession(callback: {(session: Session, success: Bool, message: String) in
//            XCTAssertTrue(success)
//            
//            myExpectation.fulfill()
//        })
//        
//        waitForExpectations(timeout: 60) { error in
//            if let error = error {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//        
//    }
//    
//    func testMyTask() {
//        
//        let myExpectation = expectation(description: "dopo aver effettuato il login, viene restituita la lista di task aperti dell'utente")
//        
//        restClient.myTask(page: self.page, numberPerPage: self.numberPerPage, userId: self.userId, callback: {(success: Bool, message: String) in
//            XCTAssertTrue(success)
//            
//            myExpectation.fulfill()
//        })
//        
//        waitForExpectations(timeout: 60) { error in
//            if let error = error {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//        
//    }
    
//    func testLogout() {
//        
//        let myExpectation = expectation(description: "dopo aver effettuato il login, viene effettuato il logout")
//        
//        restClient.logout(callback: {(success: Bool, message: String) in
//            XCTAssertTrue(success)
//            
//            myExpectation.fulfill()
//        })
//        
//        waitForExpectations(timeout: 5) { error in
//            if let error = error {
//                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
//            }
//        }
//        
//    }
    
}
